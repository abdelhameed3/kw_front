import { TranslateService } from '@ngx-translate/core';
import { SwiperOptions } from 'swiper';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notebook',
  templateUrl: './notebook.component.html',
  styleUrls: ['./notebook.component.scss'],
})
export class NotebookComponent implements OnInit {
  public config2: SwiperOptions = {};

  sliderImage = [];

  selectedImage;
  constructor(public translate: TranslateService) {
    this.sliderImage = [
      './assets/images/slider_image.png',
      './assets/images/pro8.png',
      './assets/images/pro7.png',
      './assets/images/pro6.png',
      './assets/images/pro5.png',
      './assets/images/pro8.png',
      './assets/images/pro7.png',
      './assets/images/pro6.png',
      './assets/images/pro5.png',
    ];
    this.selectedImage = this.sliderImage[0];
  }

  ngOnInit(): void {
    this.config2 = {
      a11y: { enabled: true },
      keyboard: true,
      mousewheel: false,
      scrollbar: false,
      pagination: false,
      centeredSlides: false,

      loop: true,
      navigation: {
        nextEl: '.Products_content .slider_control .right_arrow',
        prevEl: '.Products_content .slider_control .left_arrow',
      },
      breakpoints: {
        0: {
          slidesPerView: 3,
          spaceBetween: 15,
          direction: 'horizontal',
        },
        640: {
          slidesPerView: 3,
          spaceBetween: 15,
          direction: 'horizontal',
        },
        767: {
          slidesPerView: 3,
          spaceBetween: 15,
          direction: 'horizontal',
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
          direction: 'vertical',
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 30,
          direction: 'vertical',
        },
      },
    };
  }
  getImage(src) {
    this.selectedImage = src;
  }
}
